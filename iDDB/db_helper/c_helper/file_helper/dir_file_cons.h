// positive constants
#define TRUE 1
#define SUCCESS 1
#define SUCCESS_STR "SUCCESS"

// negative constants
#define FALSE 0
#define FAILURE 0
#define FAILURE_STR "FAILURE"

// general usage
#define MAX_STREAM_LENGTH 102400
#define MIN_STREAM_LENGTH 1024
